/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author PlugPC
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> getProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"espeso1",10,"1.jpg"));
        list.add(new Product(2,"espeso2",20,"2.jpg"));
        list.add(new Product(3,"espeso3",30,"1.jpg"));
        list.add(new Product(4,"nes1",40,"2.jpg"));
        list.add(new Product(5,"nes2",10,"1.jpg"));
        list.add(new Product(6,"nes3",20,"2.jpg"));
        list.add(new Product(7,"blackcoffe1",30,"1.jpg"));
        list.add(new Product(8,"blackcoffe2",40,"2.jpg"));
        list.add(new Product(9,"blackcoffe3",50,"1.jpg"));
        list.add(new Product(10,"eiei",1500,"VIP.jpg"));
        return list;
    }
}
